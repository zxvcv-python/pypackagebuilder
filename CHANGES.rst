Changelog
=========

0.3.0 (2023-03-12)
- Clean dependencies lists in tox.ini and pyproject.toml.
- README updates.
- Add dir/output parameter to commands.
- Pushing only single whl file for py3 as package is not precompiled.

0.2.3 (2023-03-12)
- Removed dependency to zxvcv.util.

0.2.2 (2023-03-12)
- Removed isort from pre-commit checks.

0.2.0 (2023-03-12)
- Add commands: build, compile, check, publish.

0.1.0 (2023-03-12)
- Initial commit.
