# pypackagebuilder
Building commands for python packages.

Has feature to:
- build
- compile (to .pyc files)
- check (.whl files)
- publish
